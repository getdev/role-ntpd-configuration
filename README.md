Role Name
=========

Install ntpd as a server.

Requirements
------------

If any other time client or time server is installer, it must be remove or deactivated before using this role

Role Variables
--------------

role-ntpd-configuration:
  sources:
    - server1
    - server2
    # Default with debian server

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

GPLv3

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
